//
//  ImageCell.swift
//  Image Gallery
//

import UIKit

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var aspectRatio: CGFloat?
    
    var URL: URL? {
        didSet {
            if let url = URL {
                imageView.image = nil
                spinner.startAnimating()
                NetworkManager.loadImage(with: url) { [weak self] image in
                    if url == self?.URL {
                        self?.spinner.stopAnimating()
                        self?.imageView.image = image
                    }
                }
            }
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        layoutAttributes.bounds.size.height = layoutAttributes.size.width / (aspectRatio ?? 1)
        return layoutAttributes
    }
}

extension UIImage {
    var aspectRatio: CGFloat {
        return self.size.width / self.size.height
    }
}
