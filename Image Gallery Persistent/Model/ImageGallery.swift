//
//  ImageGallery.swift
//  Image Gallery
//

import Foundation
import UIKit

struct ImageModel: Equatable, Codable {
    var url: URL
    var aspectRatio: CGFloat
}

struct ImageGallery: Equatable, Codable {
    var name: String

    var images = [ImageModel?]()
    
    var json: Data? {
        return try? JSONEncoder().encode(self)
    }

    init(_ name: String = "Default") {
        self.name = name
    }
    
    init?(json: Data) {
        guard let newValue = try? JSONDecoder().decode(ImageGallery.self, from: json) else {
            return nil
        }
        
        self = newValue
    }
}
