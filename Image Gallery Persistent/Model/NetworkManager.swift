//
//  NetworkManager.swift
//  Image Gallery
//
//  Created by paul on 14/04/2020.
//  Copyright © 2020 Stanford University. All rights reserved.
//

import Foundation
import UIKit

class NetworkManager {
    static func loadImage(with url: URL, completeHandler: @escaping (UIImage) -> ()) {
        let request = URLRequest(url: url)
        DispatchQueue.global(qos: .userInitiated).async(execute: {
            if let data = URLCache.shared.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    completeHandler(image)
                }
            } else {
                let session = URLSession(configuration: URLSession.shared.configuration)
                let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
                    DispatchQueue.main.async {
                        var image = UIImage(systemName: "xmark.octagon")
                        if let imageData = data, let response = response, let uploadedImage = UIImage(data: imageData) {
                            image = uploadedImage
                            //let cachedData = CachedURLResponse(response: response, data: imageData)
                            //URLCache.shared.storeCachedResponse(cachedData, for: request)
                        }
                        
                        completeHandler(image!)
                    }
                    
                })
                task.resume()
            }
        })
    }
}
